package com.company;
// modifiers class nameOfclass {
//   field_modifier field_type field_name = initialValue;
//
//   constructor used for class fields initialization
//     constructor_modifier nameOfclass(){
//
//      }

//     method_modifier return_type method_name(method_parameters){
//
//     return valueOfreturn_type;
//     }
//}
// access modifier:
// public - visible from everywhere
// private protected (default)
// outer class
public class Student extends Person {
    long studentId;


    // overloading
    // constructor 1
    public Student(){

    }

    // constructor 2 - constructor overloading
    public Student(long id){
            studentId = id;
    }

    @Override
    void earnMoney() {
        System.out.println("i get help from family");
    }
// method overloading
    void earnMoney(int amount) {
        System.out.println("i get help from family $"+amount);
    }

    // name of method in child and parent are same
    // overriding
    public void speak(){
        System.out.println("I am good student");
    }
    // inner class
    protected class CreditBook{

    }

    public void studying(){
        System.out.println("im am studying");
    }
}

