package com.company;


// modifiers nameOfclass {
//   field_modifier field_type field_name = initialValue;
//
//   constructor used for class fields initialization
//     constructor_modifier nameOfclass(){
//
//      }

//     method_modifier return_type method_name(method_parameters){
//
//     return valueOfreturn_type;
//     }
//}

public class Main {
// unified modelling language UML
    //
    public static void main(String[] args) {
	Student andriy = new Student();
	andriy.studying();

	Student charles = new Student();
	charles.studying();
	Student augustine = new Student(400L);

	Staff labworker = new Staff();
	labworker.speak();
	labworker.earnMoney();
	andriy.earnMoney();
	charles.earnMoney();
	augustine.earnMoney(100);


    }
}
